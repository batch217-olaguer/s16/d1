console.log("Hello World!");

// [SECTION] Arithmetic Operators
// +,-,*,/,%

let x = 3;
let y = 10;

let sum = x+y;
console.log("Result of addition operator: " +sum);

let difference = x-y;
console.log("Result of subtraction operator: " +difference);

let product = x*y;
console.log("Result of multiplication operator: " +product);

let quotient = x/y;
console.log("Result of division operator: " +quotient);

// to check if the number is an Even number.
let remainder= y%x;
console.log("Result of Modulo Operator: " +remainder);

// [SECTION]
// Assignment Operator
// Basic Assigning Operator is (=) equal sign.

let assignmentNumber = 8;


// Addition Assignemnt
assignmentNumber=assignmentNumber + 2;
console.log("Result of addition assignment operator: "+assignmentNumber);

assignmentNumber+=3;
console.log("Result of addition assignment operator: "+assignmentNumber);
assignmentNumber/=2;
console.log("Result of division assignment operator: "+assignmentNumber);
assignmentNumber*=4;
console.log("Result of multiplication assignment operator: "+assignmentNumber);
assignmentNumber-=7;
console.log("Result of subtraction assignment operator: "+assignmentNumber);

// Multiple Operators and Parentheses

let mdas = 1+2-3*4/5;
console.log("Result of mdas operator: "+mdas);

let	pemdas = 1+(2-3)*(4/5);
console.log("Result of pemdas operator: "+pemdas);
let a0909090 = pemdas+1;
console.log(a0909090);

// Incrementation and Decrementation
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z=7;
// pre-increment
let increment= ++z; //adds 1 to original value making it the new +decrement value
console.log("Result of pre-increment: "+increment); //=8
console.log("Result of pre-increment: "+z); //=8

// Post-Incrementation
let l=5;
incrementl= l++;
incrementl= l++; //inc= originalValue+1 = new original value.
console.log("Result of post-increment: "+incrementl); //inc=5+1=6=new l (original value keeps new value after increment.)
console.log("Result of post-increment: "+l); //original value + 1=6

// Decrementation
// Pre-decrementation
let c=4;
let decrementc= --c; //decrement value is subtracted by 1 and original value is subtracted by 1.
console.log("Result of pre-decrementation: "+decrementc);
console.log("Result of pre-decrementation: "+c);

// post-dec
let v=3;
decrementv= v--; //minus 1 from value keeps previous value. decrement value is 3 but  original value is v-1=2
console.log("Result of post-decrementation: "+decrementv) //=3
console.log("Result of post-decrementation: "+v); //=2

// [SECTION] Type Coercion
// type coercion is the automatic or implicit conversion of values from one data type to another

let numA='10';
let numB= 12;

let coercion= numA+numB;
console.log(coercion);
console.log(typeof coercion);

let numC= 16;
let numD= 14;
let nonCoercion = numC+numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

let numE= true + 1; //True = 1, so 1+1= 2
console.log(numE);

let numF= false + 1; //False = 0, so 0+1=1
console.log(numF);

// [SECTION] Comparison Operatos
let juan= 'juan';

// Equality Operator (==)
// = assigntment of value
// ==Comparison Equality
console.log(1==1);
console.log(1==2);
console.log(1=='1');
console.log(0==false);
console.log("juan"=="juan");
console.log("juan"==juan);

// Inequality Operator (!=)
// !=not
console.log(1!=1);
console.log(1!=2);

// Strict Equality Operator (===)
// checks if the value or operand are equal and are of the same type.
console.log(1==='1');
console.log("juan"===juan);
console.log(0===false);

// Strinct Inequality Operator
console.log(1!=='1');
console.log("juan"!==juan);
console.log(1!==false);

// [SECTOIN] Relational Operator
// Some comparison operators check whether one value is greater or less than to the other value.
//  >, <, =

let a=50;
let b=65;
let isGreaterThan= a>b;
let isLessThan=a<b;
let isGTorEqual=a>=b;
console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);

let isLTorEqual= a<=b;
console.log(isLTorEqual);

// [SECTION] Logical Operators
// &&-->AND, ||-->OR, !-->NOT

let isLegalAge=true;
let isRegistered=false;
let allRequirementsMet= isLegalAge && isRegistered;
console.log("Logical && Result: " +allRequirementsMet);

let someRequirementsMet= isLegalAge || isRegistered;
console.log("Logical || Result: " +someRequirementsMet);

let someRequirementsNotMet= !isRegistered;
console.log("Logical ! Result: " +someRequirementsNotMet);